from trainerbase.memory import Address, allocate


weapon_base_address = Address(allocate())

waypoint_coords_address = Address(allocate(), [0x0])
waypoint_x = waypoint_coords_address
waypoint_y = waypoint_coords_address + 0x4
waypoint_z = waypoint_coords_address + 0x8
