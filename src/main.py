from trainerbase.main import run

from gui import run_menu
from injections import update_waypoint_coords_address, update_weapon_base_pointer


def on_initialized():
    update_weapon_base_pointer.inject()
    update_waypoint_coords_address.inject()


if __name__ == "__main__":
    run(run_menu, on_initialized)
