from trainerbase.codeinjection import AllocatingCodeInjection
from trainerbase.process import pm

from memory import (
    waypoint_coords_address,
    weapon_base_address,
)


update_weapon_base_pointer = AllocatingCodeInjection(
    pm.base_address + 0x1148A24,
    f"""
        push rax
        mov rax, {weapon_base_address.base_address}
        lea r13, [rdx + r14 + 0x518]
        mov [rax], r13
        pop rax

        mov r13d, [rdx + r14 + 0x518]
        mov r15d, [rdx + r14 + 0x51C]
    """,
    original_code_length=16,
)

update_waypoint_coords_address = AllocatingCodeInjection(
    pm.base_address + 0x10A0917,
    f"""
        movss [rsi + rcx * 0x8 + 0x10], xmm1
        shl r13d, 0x10
        movsd [rsi + rcx * 0x8 + 0x14], xmm0

        push rax
        push rbx

        mov rax, {waypoint_coords_address.base_address}
        lea rbx, [rsi + rcx * 0x8 + 0x14]
        mov [rax], rbx

        pop rbx
        pop rax
    """,
    original_code_length=16,
)
