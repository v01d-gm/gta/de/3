from trainerbase.gameobject import GameFloat, GameInt
from trainerbase.memory import Address
from trainerbase.process import pm

from memory import (
    waypoint_x,
    waypoint_y,
    waypoint_z,
    weapon_base_address,
)


money = GameInt(pm.base_address + 0x516E158)

total_ammo = GameInt(weapon_base_address + [0x4])
ammo_in_clip = GameInt(weapon_base_address + [0x0])

# Possible player base static offsets:
#   - 0x4D9FB48
#   - 0x4D9FC88
#   - 0x4D9FF10
#   - 0x514E198
#   - 0x516E060
#   - 0x5048EA0

player_base_address = Address(pm.base_address + 0x4D9FB48)
health = GameFloat(player_base_address + [0x430])

player_coords_address = player_base_address + [0x38]
player_x = GameFloat(player_coords_address)
player_y = GameFloat(player_coords_address + 0x4)
player_z = GameFloat(player_coords_address + 0x8)

waypoint_x = GameFloat(waypoint_x)
waypoint_y = GameFloat(waypoint_y)
waypoint_z = GameFloat(waypoint_z)
