import dearpygui.dearpygui as dpg
from trainerbase.common.keyboard import SimpleHotkeyHandler
from trainerbase.gui.helpers import add_components, simple_trainerbase_menu
from trainerbase.gui.misc import HotkeyHandlerUI, SeparatorUI, TextUI
from trainerbase.gui.objects import GameObjectUI
from trainerbase.gui.speedhack import SpeedHackUI
from trainerbase.gui.teleport import TeleportUI
from trainerbase.tts import say

from objects import ammo_in_clip, health, money, total_ammo, waypoint_x, waypoint_y, waypoint_z
from teleport import tp


def on_hotkey_goto_waypoint_press():
    # When teleporting in a car, spawning underground will avoid rotation.
    # When teleporting as a pedestrian, you need to appear a little higher,
    # since the waypoint mark is one unit below the surface.
    drop_height = 1 if health.value else -700

    tp.goto_waypoint(drop_height)
    say("Teleported to waypoint")


@simple_trainerbase_menu("Grand Theft Auto III – The Definitive Edition", 750, 450)
def run_menu():
    with dpg.tab_bar():
        with dpg.tab(label="Main", tag="objects"):
            add_components(
                GameObjectUI(health, "Health", "Shift+F1", "F1", default_setter_input_value=1000),
                GameObjectUI(money, "Money", default_setter_input_value=5_000_000),
                GameObjectUI(total_ammo, "Total Ammo", "Shift+F2", "F2", default_setter_input_value=9999),
                GameObjectUI(ammo_in_clip, "Ammo In Clip", "Shift+F3", "F3", default_setter_input_value=100),
                SeparatorUI(),
                SpeedHackUI(),
            )

        with dpg.tab(label="Teleport"):
            add_components(
                TeleportUI(tp, "Insert", "PageDown", "End"),
                SeparatorUI(),
                TextUI("Waypoint"),
                HotkeyHandlerUI(SimpleHotkeyHandler(on_hotkey_goto_waypoint_press, "PageUp"), "Go To Waypoint"),
                GameObjectUI(waypoint_x, "Waypoint X", before_set=float),
                GameObjectUI(waypoint_y, "Waypoint Y", before_set=float),
                GameObjectUI(waypoint_z, "Waypoint Z", before_set=float),
            )
