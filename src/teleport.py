from trainerbase.common.helpers import suppress_memory_exceptions
from trainerbase.common.teleport import Teleport

from objects import player_x, player_y, player_z, waypoint_x, waypoint_y, waypoint_z


class Gta3Teleport(Teleport):
    @suppress_memory_exceptions
    def goto_waypoint(self, drop_height: float):
        self.set_coords(waypoint_x.value, waypoint_y.value, waypoint_z.value + drop_height)


tp = Gta3Teleport(
    player_x,
    player_y,
    player_z,
    {
        "espresso2go_stall_1": (-1262.2396240234375, -91.6584701538086, 58.72998809814453),
        "espresso2go_stall_2": (-657.8917236328125, -725.1267700195312, 18.86794662475586),
        "espresso2go_stall_3": (32.59532165527344, -870.8571166992188, 33.132816314697266),
        "espresso2go_stall_4": (73.73194885253906, -1337.16455078125, 26.150405883789062),
        "espresso2go_stall_5": (29.926477432250977, -1152.5401611328125, 26.12809944152832),
        "espresso2go_stall_6": (303.56341552734375, -1510.4783935546875, 23.65961265563965),
        "espresso2go_stall_7": (1329.54248046875, -806.08251953125, 14.985172271728516),
        "espresso2go_stall_8": (1336.888427734375, -253.1158905029297, 49.596778869628906),
    },
)
